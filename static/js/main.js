let author_input = document.getElementById('author');
let joke_input = document.getElementById('joke');
let reset_btn = document.getElementsByClassName("reset")[0]
 let generate_btn = document.getElementsByClassName("generate")[0];
 let submit = document.getElementsByClassName("submit")[0];
let authorErrorInput = document.getElementsByClassName('errorInput')[0];
let jokeErrorInput = document.getElementsByClassName('errorInput')[1];


generate_btn.addEventListener('click' ,() => {
  let xhr = new XMLHttpRequest;
  xhr.open("get","https://api.chucknorris.io/jokes/random", true);
  xhr.addEventListener("load", () => {
    if (xhr.status != 200) return alert("error" + xhr.response);
    let generatedData = JSON.parse(xhr.response)
    author_input.value = "chucknorris";
    joke_input.value = generatedData.value;
    verification();
  });
  xhr.addEventListener("error", () => {
    alert("error");
  });
  xhr.send();

})



const url = "http://localhost:3000/jokes";
let ul = document.getElementsByClassName('jokesList')[0];

let data = {
    likes:12,
    author:"Aymen",
    joke:"Wow bro foe",
}


let verification =  () => {
  let author = author_input.value;
  let joke = joke_input.value;

  if (!author) {
    authorErrorInput.innerText = "author is required";
      submit.setAttribute("disabled", "");
  }

  if (!joke || joke.length <= 5) {
    jokeErrorInput.innerText =
      "Joke is required and must be at least 5 caracters";
       submit.setAttribute("disabled", "");
  }

  authorErrorInput.innerText = "";
  jokeErrorInput.innerText = "";
  submit.removeAttribute("disabled");
};




author_input.addEventListener('change', ()=>{
  verification();
})

joke_input.addEventListener('change', ()=>{
  verification();
})




reset_btn.addEventListener("click", () => {
   author_input.value = ""
   joke_input.value = ""
});


generate_btn.addEventListener("click", () => {
  console.log("hello sow")
});

submit.addEventListener("click", () => {
  let authorValue = author_input.value;
  let jokeValue =joke_input.value;
  let like = 0;
  let dataToSend = {
    author: authorValue,
    joke: jokeValue,
    likes: like
  };
  dataToSend = JSON.stringify(dataToSend);
  const xhr = new XMLHttpRequest();
  xhr.open("post", url, true);
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.addEventListener("load", () => {
    if (xhr.status == 201) {
      let data = JSON.parse(xhr.response);
      MakeDivJoke(data);
      authorValue.value = "";
      jokeValue.value = "";
    } else {
      alert(xhr.response);
    }
  });
  xhr.addEventListener("error", () => {
    alert("error");
  });
  xhr.send(dataToSend);
});



let MakeDivJoke = (data)=>{
     
     let li = document.createElement('li');
     let div1 = document.createElement('div');
     let div2 = document.createElement('div');
     let div3 = document.createElement('div');
     let h3 = document.createElement('h3');
     let p = document.createElement('p');
     let btn_delete = document.createElement('button');
     let btn_like = document.createElement('button');
      
        li.appendChild(div1);
        li.appendChild(div2);
        li.appendChild(div3);
        div2.appendChild(h3);
        div2.appendChild(p);
        div3.appendChild(btn_delete);
        div3.appendChild(btn_like);
        div1.classList.add('likes');
        div2.classList.add('content');
        div3.classList.add('btns');
        btn_delete.classList.add('delete');
        btn_like.classList.add('likeBtn');
        

        div1.textContent = data.likes + " likes";
        h3.textContent = data.author;
        p.textContent = data.joke;
        btn_delete.textContent = 'delete';
        btn_like.textContent = 'like';

        btn_delete.addEventListener('click',()=>{
          const xhr = new XMLHttpRequest();
          xhr.open("delete", url + "/" + data.id, true);
          xhr.addEventListener("load", () => {
          if (xhr.status != 200) return alert("error" + xhr.response);
          li.remove()
         });
          xhr.addEventListener("error", () => {
           alert("error");
            });
           xhr.send()
        })
        let ds
        
        btn_like.addEventListener('click', ()=>{
          const xhr = new XMLHttpRequest();
          xhr.open("put", url + "/" + data.id, true);
          xhr.setRequestHeader("Content-Type","application/json")
          xhr.addEventListener("load", () => {
            if (xhr.status != 200) return alert("error" + xhr.response);
          });

          xhr.addEventListener("error", () => {
            alert("error");
          });
          let dataPutToSend = {
            id: data.id,
            author: data.author,
            joke: data.joke,
            likes: data.likes+ 1
          };
          ds = data.likes+1;
            div1.textContent = ds + " likes";
          xhr.send(JSON.stringify(dataPutToSend))
        })
        ul.appendChild(li);
}

const loadJokes = () => {
   ul.innerHTML = "";
    const xhr = new XMLHttpRequest();
    xhr.open("get", url, true);
    xhr.addEventListener("load", () => {
      if (xhr.status != 200) return alert("error" + xhr.response);
      let data = JSON.parse(xhr.response);
      data.forEach((ele) => MakeDivJoke(ele));
    });
    xhr.addEventListener("error", () => {
      alert("error");
    });
    xhr.send();
  };



  loadJokes()

  ///find de projs