// min 4 char

let express = require('express');   
let app = express();
let port = 3000;
let jokes = require('./database/db.json');
let fs = require('fs');
const exp = require('constants');
app.use(express.static("./static"));
app.use(express.json())


app.get("/jokes",(req,res)=>{
    fs.readFile("./database/db.json",(err,data)=>{
        if(err)
            return res.status(500).send("error on the server")
        let jokes_data = JSON.parse(data.toString()).jokes;
        res.status(200).json(jokes_data)
    });
})

app.post("/jokes",(req,res)=>{
    let {author, joke,likes} = req.body
    fs.readFile("./database/db.json",(err,data)=>{
        if(err)
            return res.status(500).send("error on the server")
        let data_ = JSON.parse(data.toString())
        let JokeSaving = {
            id:data_.last_id,
            author: author,
            joke:joke,
            likes:likes,
        }
        data_.jokes.push(JokeSaving);
        data_.last_id++
        fs.writeFile("./database/db.json",JSON.stringify(data_,null,4),(err)=>{
            if(err)
                return res.status(500).send("error on the server")
            res.status(201).json(JokeSaving)
        })
    });
})



app.delete("/jokes/:id",(req,res)=>{
    let {id} = req.params
    //let id = req.params.id
    fs.readFile("./database/db.json",(err,data)=>{
        if(err)
            return res.status(500).send("error on the server")
        let dataFile = JSON.parse(data.toString())
        let Jokes_ = dataFile.jokes
        let JokeIndex = Jokes_.findIndex(ele=>ele.id==id)
        if(JokeIndex==-1)
            return res.status(404).send("Joke Not found not found")
        dataFile.jokes = Jokes_.filter(ele=>ele.id!=id)
        fs.writeFile("./database/db.json",JSON.stringify(dataFile,null,4),(err)=>{
            if(err)
                return res.status(500).send("error on the server")
            res.status(200).json("joke is deleted with success")
        })
    });

})



app.put("/jokes/:id",(req,res)=>{
    let {id} = req.params
    fs.readFile("./database/db.json",(err,data)=>{
        if(err)
            return res.status(500).send("error on the server")
        let dataFile = JSON.parse(data.toString())
        let Jokes_ = dataFile.jokes;
        let JokeData= Jokes_.find(ele=>ele.id==id)
        if(!JokeData)
            return res.status(404).send("question not found")
       let {author,joke, likes} = req.body
       JokeData.author = author
       JokeData.joke = joke
       JokeData.likes = likes;
       fs.writeFile("./database/db.json",JSON.stringify(dataFile,null,4),(err)=>{
        if(err)
            return res.status(500).send("error on the server")
        res.status(200).json(JokeData)
    })
    });
})


app.listen(port,()=>{
    console.log("The Server is Running in port 3000")
})